'use strict'

const chai = require('chai');
const chaiHttp = require('chai-http');
const console = require('../../log');
const server = require('../../app');

let expect = chai.expect;

chai.use(chaiHttp);

describe('Distribuidores', function () {
    let token = '';
    describe('POST /app/dist/login', function () {
        it('200 ok and contains token', done => {
            chai.request(server)
                .post('/app/dist/login')
                .send({
                    cc: "5678912345",
                    password: "123456Clon"
                })
                .end(function (err, res) {
                    token = res.body.token;
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('token');
                    done();
                })
        });
    });
    describe('GET /app/dist', function () {
        console.log('token: ' + token);
        it('200 ok and json data format', done => {
            chai.request(server)
                .get('/app/dist')
                .set('Authorization', token)
                .end(function (err, res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an('array');
                    done();
                })
        });
    });

    // describe('POST /register', function () {
    //     it('200 OK and json response', done => {
    //         chai.request(server)
    //             .post('/api/dist/register')
    //             .end(function (err, res) {
    //                 expect(res).to.have.status(200);
    //                 done();
    //             });
    //     });
    // });

});
